# HPrSendMailStateChange

Set the "send mail to customer" checkbox on order stage cahnges to false as the default
value.

## Effect

### Before
![without plugin](./imgs/act.png)

### After
![with plugin](./imgs/inact.png)

## Install Plugin via Composer

```
composer require hannespries/sendmailstatechange
```

## Build the Plugin
1. Fork the Git repository
   * on Gitlab with Gitlab-CI create a tag starting with **release** like **release0.0.1**
2. Build with Phing (or Ant)
    ```
    ./vendor/bin/phing copy -Dcopy-to=HPrSendMailStateChange
    ```
3. Zip this folder and install the plugin
4. Copy this folder to custom/plugins/