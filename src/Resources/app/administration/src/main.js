const { Component, Mixin } = Shopware;

Component.override('sw-order-state-change-modal-attach-documents', {
    data() {
        return {
            sendMail: false,
        };
    }
});